var myApp = angular.module("myApp",['ngCsvImport']);

myApp.controller('myCtrl',["$scope",function($scope){

	$scope.validateData=function(data){


	var fields_in_sheet=["student_firstname","student_lastname","student_ID","period","subject","email","phone","teacher_firstname",
				"teacher_lastname","teacherid","schoolname","schoolid","districtname","districtid","statename","stateid"];
	var count=0;
		angular.forEach(data[0],function(v,k){
			if(fields_in_sheet.indexOf(k) > -1){
				count ++;
			}
		});

		if(count==0){
			alert('Incorrect sheet format');return;
		}

		angular.forEach(data,function(val,key){

			$scope.validate_required(val);
		});
		console.log(data);
		$scope.filtered_result=data;
	}

	$scope.validate_required=function(obj){


		    var validation_rules={
		    		"student_firstname" :["required","notNUll",'text',512],
		    		"student_lastname"  :["required","notNUll",'text',512],
	    			"teacher_firstname" :["required","notNUll",'text',512],
					"teacher_lastname"  :["required","notNUll",'text',512],
					"schoolname"		:["required","notNUll",'text',512],
					"districtname"		:["required","notNUll",'text',512],
					"statename"			:["required","notNUll",'text',512]
		    }
		    this.alias_required=' ';
		    this.alias_notNull=null;
		    this.alias_null_as_string='null';
		    this.max_length='max-length'
		    var is_exists=false;

		    angular.forEach(validation_rules,function(v,k){
		    		
		    	is_exists=$scope.check_for_required(obj,k);
		    	
		    	if(is_exists){
	    			if(!obj[k] || obj[k]=='undefined' ){
		    			
		    			obj[k+'_error']=1;
		    			obj[k+'_error_message']="This Cannot Be Empty";
		    			
		    			
		    		}
		    		if(obj[k]==this.alias_required){
		    			obj[k+'_error']=1;
		    			obj[k+'_error_message']="This Cannot Be Empty";
		    			
		    		}

		    		if(obj[k]==this.alias_notNull){
		    			obj[k+'_error']=1;
		    			obj[k+'_error_message']="This Cannot Be Null";
		    			
		    		}
		    		if(obj[k]==this.alias_null_as_string){
		    			obj[k+'_error']=1;
		    			obj[k+'_error_message']="This Cannot Be Null";
		    			
		    		}

		    	}
		    	else{
		    			obj[k+'_error']=1;
		    			obj[k+'_error_message']="This Cannot Be Empty";

		    	}
		    	
		    		
		    		
		    });



		}

 $scope.check_for_required=function(obj,key){

 	if(!obj[key]){
 		return false;
 	}
 	else{
 		return true;
 	}
 }


}]);