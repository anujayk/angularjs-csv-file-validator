<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=1024, initial-scale=1">	
	<link rel="stylesheet" type="text/css" href="bower_components/hint.css/hint.css">
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
	
	
	<link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body ng-app="myApp" ng-controller="myCtrl">


<div class="container-fluid">
   <div class="row">
	    <div class="col-md-3">
		  <div class="pull-right">
		  		<ng-csv-import content="csv.content" header="true" separator="','"  result="csv.result"  accept="'csv.accept'">
    			</ng-csv-import>
		  </div>
		</div>
	  <div class="col-md-3 grid-total">
	  		 <div>Total</div>
	  		 <div>{{50+50}}</div>
	  </div> 
	  <div class="col-md-3 grid-pass">
	  		<div>Pass</div>
	  		<div>100</div>
	  </div>
	  <div class="col-md-3 grid-fail">
	  		<div>Fail</div>
	 		<div ng-if="csv.result.length > 0">{{csv.result.length}}
	 			<div ng-init="validateData(csv.result)"></div>
	 		</div>
	  </div>
	</div>
</div>
   
	<div class="container-fluid">
    	<div class="row">
        	<div class="col-md-12">
	          	<div class="panel-body">
			    	<table class="table table-striped">
					    <thead>
					         <tr>
					            <th>#</th>
					            <th >Student_firstname</th>
					            <th>Student_lastname</th>
					            <th>studentid</th>
					            <th>period</th>
					            <th>subject</th>
					            <th>email</th>
					            <th>phone</th>
					            <th>teacher_firstname</th>
					            <th>teacher_lastname</th>
					            <th>teacherid</th>
					            <th>schoolname</th>
					            <th>schoolid</th>
					            <th>districtname</th>
					            <th>districtid</th>
					            <th>statename</th>
					            <th>stateid</th>
					            <th>edit</th>
					         </tr>
					    </thead>
					    <tbody >
					         <tr ng-repeat="r in filtered_result">
					            <td>{{$index+1}}</td>
					            <td ng-class="{'danger':r.student_firstname_error==1}"><span  ng-class="{'hint--top  hint--error':r.student_firstname_error==1}" data-hint="{{r.student_firstname_error_message}}" >{{r.student_firstname}}</span></td>
					            <td ng-class="{'danger':r.student_lastname_error==1}"><span  ng-class="{'hint--top  hint--error':r.student_lastname_error==1}" data-hint="{{r.student_lastname_error_message}}" >{{r.student_lastname}}</td>
					            <td>{{r.student_ID}}</td>
					            <td>{{r.Period}}</td>
					            <td>{{r.subject}}</td>
					            <td>{{r.email}}</td>
					            <td >{{r.student_cellphone}}</td>
					            <td>{{r.teacher_firstname}}</td>
					            <td>{{r.teacher_lastname}}</td>
					            <td >{{r.teacher_ID}}</td>
					            <td>{{r.school_name}}</td>
					            <td>{{r.school_ID}}</td>
					            <td >{{r.district_name}}</td>
					            <td >{{r.district_ID}}</td>
					            <td>{{r.statename}}</td>
					            <td >{{r.stateid}}</td>
					            <td>
					            	<button class="btn btn-primary" type="submit">
					            		<i class="glyphicon glyphicon-edit"></i>
					                </button>
					            </td>
					         </tr>
					    </tbody>
					</table>
				</div>
	  		</div>
		</div>
	</div>

   

<script type="text/javascript" src="bower_components/angular/angular.js"></script>


<script src="bower_components/angular-csv-import-tmp/dist/angular-csv-import.js"></script>
<script type="text/javascript" src="js/myApp.js"></script>
</body>
</html>